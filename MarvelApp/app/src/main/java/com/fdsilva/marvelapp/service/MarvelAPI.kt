package com.fdsilva.marvelapp.service

import com.fdsilva.marvelapp.BuildConfig
import com.fdsilva.marvelapp.model.Response
import com.fdsilva.marvelapp.utils.BASE_URL
import com.fdsilva.marvelapp.utils.getApiHash
import com.fdsilva.marvelapp.utils.getTimeStamp
import com.google.gson.GsonBuilder
import io.reactivex.Observable
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface MarvelAPI {
    @GET("characters")
    fun getAllCharacters(@Query("offset") offset: Int? = 0): Observable<Response>

    companion object {
        fun getService(): MarvelAPI {
            val httpClient = OkHttpClient.Builder()
            val timeStamp = getTimeStamp()
            httpClient.addInterceptor {
                val request = it.request()
                val requestURL = request.url()


                val url = requestURL.newBuilder()
                    .addQueryParameter("ts", timeStamp)
                    .addQueryParameter("apikey", BuildConfig.PUBLIC_API_KEY)
                    .addQueryParameter("hash", getApiHash(timeStamp))
                    .build()
                it.proceed(request.newBuilder().url(url).build())
            }

            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().setLenient().create()))
                .client(httpClient.build())
                .build()

            return retrofit.create<MarvelAPI>(MarvelAPI::class.java)
        }
    }
}