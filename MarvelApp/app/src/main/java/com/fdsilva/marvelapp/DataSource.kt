package com.fdsilva.marvelapp

import android.util.Log
import androidx.paging.DataSource
import androidx.paging.PageKeyedDataSource
import com.fdsilva.marvelapp.model.Character
import com.fdsilva.marvelapp.service.MarvelAPI
import io.reactivex.disposables.CompositeDisposable

class DataSource(
    private val api: MarvelAPI,
    private val compositeDisposable: CompositeDisposable
): PageKeyedDataSource<Int, Character>() {
    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Character>
    ) {
        getCharacter(0, 1, params.requestedLoadSize, callback, null)
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Character>) {
        getCharacter(params.key, params.key + 1, params.requestedLoadSize, null, callback)
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Character>) {
        getCharacter(params.key, params.key - 1, params.requestedLoadSize, null, callback)
    }

    private fun getCharacter(requestedPage: Int,
                             adjacentPage: Int,
                             loadSize: Int,
                             initialCallback: LoadInitialCallback<Int, Character>?,
                             callback: LoadCallback<Int, Character>?) {
        compositeDisposable.add(
            api.getAllCharacters(requestedPage * loadSize)
                .subscribe({
                    Log.d("DEBUG-TAG","Load page: $requestedPage")
                    initialCallback?.onResult(it.data.results, null, adjacentPage)
                    callback?.onResult(it.data.results, adjacentPage)
            }, {
                    Log.d("DEBUG-TAG","Error Loading page: $requestedPage  Message ${it.localizedMessage}")
            })
        )
    }
}

class DataSourceFactory(
    private val api: MarvelAPI,
    private val compositeDisposable: CompositeDisposable
) : DataSource.Factory<Int, Character>() {
    override fun create(): DataSource<Int, Character> = DataSource(api, compositeDisposable)
}