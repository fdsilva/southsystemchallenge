package com.fdsilva.marvelapp.utils.extensions

import android.widget.ImageView
import com.fdsilva.marvelapp.R
import com.squareup.picasso.Picasso

fun ImageView.load(url: String) {
    Picasso.get().load(url).placeholder(R.drawable.marvel_placeholder).into(this)
}