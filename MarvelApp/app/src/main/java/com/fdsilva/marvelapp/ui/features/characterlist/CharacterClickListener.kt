package com.fdsilva.marvelapp.ui.features.characterlist

import com.fdsilva.marvelapp.model.Character

interface CharacterClickListener{
    fun onRecyclerViewItemClick(character: Character)
}