package com.fdsilva.marvelapp.utils

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.fdsilva.marvelapp.R
import com.squareup.picasso.Picasso

@BindingAdapter("thumbnail")
fun loadImage(view: ImageView, url: String) {
    Picasso.get().load(url).placeholder(R.drawable.marvel_placeholder).into(view)
}