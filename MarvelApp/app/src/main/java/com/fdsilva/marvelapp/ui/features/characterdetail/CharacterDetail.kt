package com.fdsilva.marvelapp.ui.features.characterdetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.fdsilva.marvelapp.databinding.CharactersDetailLayoutBinding
import com.fdsilva.marvelapp.model.Character

private const val CHARACTER_EXTRA = "character_extra"

class CharacterDetail : Fragment() {
    private lateinit var binding: CharactersDetailLayoutBinding
    private var selectedCharacter: Character? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = CharactersDetailLayoutBinding.inflate(inflater).apply {
        binding = this
        binding.lifecycleOwner = viewLifecycleOwner
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        selectedCharacter = arguments?.getParcelable(CHARACTER_EXTRA)
        setupUi()
    }

    private fun setupUi() {
        with(binding) {
            character = selectedCharacter
            imageUrl = "${selectedCharacter?.thumbnail?.path}/portrait_incredible.${selectedCharacter?.thumbnail?.extension}"
        }
    }

    companion object {
        const val FRAGMENT_TAG = "CHARACTER_DETAIL_FRAGMENT"
        fun newInstance(character: Character) = CharacterDetail().apply {
            val args = Bundle()
            args.putParcelable(CHARACTER_EXTRA, character)
            arguments = args
        }
    }
}