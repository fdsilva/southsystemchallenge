package com.fdsilva.marvelapp.utils

import android.util.Log
import com.fdsilva.marvelapp.BuildConfig
import java.math.BigInteger
import java.security.MessageDigest
import java.util.*

private fun String.md5(): String {
    val md = MessageDigest.getInstance("MD5")
    return BigInteger(1, md.digest(toByteArray())).toString(16).padStart(32, '0')
}

fun getTimeStamp() : String {
    return (Calendar.getInstance(TimeZone.getTimeZone("UTC")).timeInMillis / 100L).toString()
}

fun getApiHash(timeStamp: String) : String {
    val hash = "$timeStamp${BuildConfig.PRIVATE_API_KEY}${BuildConfig.PUBLIC_API_KEY}".md5()
    Log.d("DEBUG-TAG", "TS: $timeStamp, hash: ${hash}")
    return hash
}