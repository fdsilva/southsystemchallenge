package com.fdsilva.marvelapp.ui.features.characterlist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.fdsilva.marvelapp.databinding.CharacterItemLayoutBinding
import com.fdsilva.marvelapp.model.Character

class CharactersListAdapter(
    private val listener: CharacterClickListener
) : PagedListAdapter<Character, CharactersListAdapter.ViewHolder>(
    diffUtil
) {
    class ViewHolder(itemView: CharacterItemLayoutBinding) : RecyclerView.ViewHolder(itemView.root) {
        var binding: CharacterItemLayoutBinding = itemView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        CharacterItemLayoutBinding.inflate(
        LayoutInflater.from(parent.context), parent, false)
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder.binding) {
            character = getItem(position)
            imageUrl = "${character?.thumbnail?.path}/landscape_xlarge.${character?.thumbnail?.extension}"

            cardFrame.setOnClickListener {
                getItem(position)?.let { it1 -> listener.onRecyclerViewItemClick(it1) }
            }
        }
    }

    companion object {
        val diffUtil = object : DiffUtil.ItemCallback<Character>() {
            override fun areItemsTheSame(oldItem: Character, newItem: Character) = oldItem.id == newItem.id
            override fun areContentsTheSame(oldItem: Character, newItem: Character) = oldItem == newItem
        }
    }
}