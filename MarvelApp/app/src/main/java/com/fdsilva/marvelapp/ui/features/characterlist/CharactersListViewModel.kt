package com.fdsilva.marvelapp.ui.features.characterlist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import androidx.paging.RxPagedListBuilder
import com.fdsilva.marvelapp.DataSourceFactory
import com.fdsilva.marvelapp.model.Character
import com.fdsilva.marvelapp.service.MarvelAPI
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

private const val PAGE_SIZE = 20
private const val INITIAL_LOAD_SIZE_HINT = PAGE_SIZE * 3
private const val PREFETCH_DISTANCE = 15

class CharactersListViewModel : ViewModel() {
    private var loadCharacterDisposable: Disposable? = null
    private val compositeDisposable = CompositeDisposable()
    private val _characterList = MutableLiveData<PagedList<Character>>()
    val characterList: LiveData<PagedList<Character>> = _characterList

    private val dataSourceFactory: DataSourceFactory

    private val pageConfig = PagedList.Config.Builder()
        .setPageSize(PAGE_SIZE)
        .setInitialLoadSizeHint(INITIAL_LOAD_SIZE_HINT)
        .setPrefetchDistance(PREFETCH_DISTANCE)
        .setEnablePlaceholders(false)
        .build()

    init {
        loadCharacterDisposable?.dispose()
        dataSourceFactory = DataSourceFactory(MarvelAPI.getService(), compositeDisposable)
        loadCharacterDisposable = RxPagedListBuilder(dataSourceFactory, pageConfig)
            .setFetchScheduler(io.reactivex.schedulers.Schedulers.io())
            .setFetchScheduler(io.reactivex.schedulers.Schedulers.io())
            .buildObservable()
            .subscribe({
                _characterList.postValue(it)
            },{

            })
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
        loadCharacterDisposable?.dispose()
    }
}