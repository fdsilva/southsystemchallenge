package com.fdsilva.marvelapp.ui.features.characterlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.paging.PagedList
import androidx.recyclerview.widget.GridLayoutManager
import com.fdsilva.marvelapp.R
import com.fdsilva.marvelapp.databinding.CharacterListLayoutBinding
import com.fdsilva.marvelapp.model.Character
import com.fdsilva.marvelapp.ui.features.characterdetail.CharacterDetail
import com.fdsilva.marvelapp.utils.extensions.observe
import kotlinx.android.synthetic.main.character_list_layout.*

private const val SPAN_COUNT = 2

class CharactersListFragment : Fragment(), CharacterClickListener {
    lateinit var binding: CharacterListLayoutBinding

    lateinit var adapter: CharactersListAdapter
    private val viewModel: CharactersListViewModel by lazy {
        ViewModelProviders.of(this).get(CharactersListViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = CharacterListLayoutBinding.inflate(inflater).apply {
        binding = this
        binding.lifecycleOwner = viewLifecycleOwner
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewModel = viewModel
        setupCharactersListUI()
        setupObservables()
    }

    private fun setupObservables() {
        with(viewModel) {
            observe(characterList, ::loadCharacterList)
        }
    }

    private fun setupCharactersListUI(){
        recyclerCharacters.layoutManager = GridLayoutManager(context, SPAN_COUNT)
        adapter = CharactersListAdapter(this)
        recyclerCharacters.adapter = adapter
    }

    private fun showDetailScreen(character: Character) {
        val characterDetail = CharacterDetail.newInstance(character)
        activity?.supportFragmentManager
            ?.beginTransaction()
            ?.replace(R.id.rootLayout, characterDetail, CharacterDetail.FRAGMENT_TAG)
            ?.addToBackStack(null)
            ?.commit()
    }

    private fun loadCharacterList(charactersList: PagedList<Character>) {
        adapter.submitList(charactersList)
    }

    companion object {
        const val FRAGMENT_TAG = "CHARACTER_LIST_FRAGMENT"
        fun newInstance() = CharactersListFragment()
    }

    override fun onRecyclerViewItemClick(character: Character) {
        showDetailScreen(character)
    }
}